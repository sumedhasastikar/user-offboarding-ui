//apigee integrated
//opt-out
export const OPT_OUT_URL = "https://apis-dev.globalpay.com/gnf/users/opt-out/"

//unsubscription from group
export const UNSUBSCRIBE_FROM_GROUP_URL = " https://apis-dev.globalpay.com/gnf/users/unsubscribe/"

//manage subscriptions
export const GET_APPLICATION_SEGREGATED_ASSOCIATION_OF_USER_GROUP_MAPPING = "https://apis-dev.globalpay.com/gnf/users/manage-subscriptions/"
export const URL_FOR_REMOVING_USER_GROUP_ASSOCIATION_IN_BULK = "https://apis-dev.globalpay.com/gnf/users/manage-subscriptions/"

//application unsubscription
export const APPLICATION_UNSUBSCRIPTION_BACKEND_URL = "https://apis-dev.globalpay.com/gnf/users/unsubscribe/application/"