import React, { Component } from 'react'
import TabularResponseComponent from '../update-preferences/TabularResponseComponent';
import { Card, CardHeader, Table } from 'reactstrap';

export class OutputComponent extends Component {
    getGroupApplicationModelData = (person) => {
        let groupApplicationModelData = []
        if (person.groupApplicationModel) {
            groupApplicationModelData = person.groupApplicationModel
        }
        return groupApplicationModelData
    }

    getUserName = (persons) => {
        return persons.userName
    }

    groupIdsAndGroupNamesRegistry = (person) => {
        let groupApplicationModelData = this.getGroupApplicationModelData(person)
        let groupIdNameMap = new Map()

        groupApplicationModelData.forEach(x => groupIdNameMap.set(x.groupId, x.groupName))
        return groupIdNameMap
    }

    renderResponseUI_2 = (data, persons) => {
        if (data !== undefined) {
            let tableData = []
            let groupIdNameMap = this.groupIdsAndGroupNamesRegistry(persons)
            data.forEach(element => {
                tableData.push(<TabularResponseComponent groupName={groupIdNameMap.get(element.groupId)} status={element.status} />)

            })
            return tableData
        } else return []
    }

    renderResultPage = (data, persons) => {
        return <React.Fragment>
            <br />
            <Card>
                <CardHeader tag="h3">Unsubscription status for {this.getUserName(persons)}</CardHeader>
                <Table>
                    <thead>
                        <tr>
                            <th>Group Name</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>{this.renderResponseUI_2(data, persons)}</tbody>
                </Table>
            </Card>
        </React.Fragment>
    }
    render() {
        if (this.props.stateData === undefined) {
            return <div>Data undefined</div>
        } else {
            return this.renderResultPage(this.props.stateData.data, this.props.persons)
        }

    }
}

export default OutputComponent
