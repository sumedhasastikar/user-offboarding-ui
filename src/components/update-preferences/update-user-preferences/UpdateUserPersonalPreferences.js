import React, { Component } from 'react'
import { Dropdown, DropdownMenu, DropdownItem, DropdownToggle, Container, Form } from 'reactstrap'

export class UpdateUserPersonalPreferences extends Component {
    constructor(props) {
        super(props);

        this.toggle = this.toggle.bind(this);
        this.state = {
            dropdownOpen: false,
            btnDropright: false,
        };
    }

    toggle() {
        this.setState({
            dropdownOpen: !this.state.dropdownOpen,
            btnDropright: !this.state.btnDropright
        });
    }
    prepareRepresentation = () => {
        return (
            <Container>
                <Form>
                    <Dropdown className='mt-3 mb-3 mr-3' direction="right" size="sm" isOpen={this.state.btnDropright} toggle={() => { this.setState({ btnDropright: !this.state.btnDropright }); }}>
                        <DropdownToggle caret>
                            Language Code
                        </DropdownToggle>
                        <DropdownMenu>
                            <DropdownItem>en-US</DropdownItem>
                            <DropdownItem>en-UK</DropdownItem>
                        </DropdownMenu>
                    </Dropdown>
                </Form>
            </Container>
        )
    }

    render() {
        return (
            <div>
                {this.prepareRepresentation()}
            </div>
        )
    }
}

export default UpdateUserPersonalPreferences
