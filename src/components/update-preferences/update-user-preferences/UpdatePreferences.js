import React, { Component } from 'react'
import UpdateUserNotificationPreferences from './UpdateUserNotificationPreferences'
import UpdateUserPersonalPreferences from './UpdateUserPersonalPreferences'
import { Alert, Spinner, Row, Col, Form, FormGroup, CardBody, Container, CustomInput, Card, CardText, CardHeader, Button } from 'reactstrap';
export class UpdateUserPreferences extends Component {
    render() {
        return (
            <Container className="mt-5">


                <Row>
                    <Col md='3'></Col>

                    <Col md='6'>

                        <Card>
                            <CardHeader tag="h3">Update User Preferences</CardHeader>
                            <CardBody>
                                <Card className='mt-2 mb-4'>
                                    <CardHeader tag="h6">User Notification Preferences</CardHeader>
                                    <UpdateUserNotificationPreferences />
                                </Card>
                                <Card className='mt-4 mb-2'>
                                    <CardHeader tag="h6">User Preferences</CardHeader>
                                    <UpdateUserPersonalPreferences />
                                </Card>
                            </CardBody>
                        </Card>

                    </Col>

                    <Col md='3'></Col>

                </Row>



            </Container>
        )
    }
}

export default UpdateUserPreferences
