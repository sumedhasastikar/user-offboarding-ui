import React from 'react'

var createReactClass = require('create-react-class');

const ErrorUtil = createReactClass({
    statics: {
      extractErrorCode:function(errors) {
        let errCode = errors.map(error => error.errorCode)
        return errCode
        },
      extractErrorMessage:function(errors) {
        let errMessage = errors.map(error => error.errorMessage)
        return errMessage
        }
    },
    render() {
    },
  });
  
  export default ErrorUtil;