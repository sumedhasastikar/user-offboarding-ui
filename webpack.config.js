var path = require("path");
var webpack = require("webpack");
var HtmlWebPackPlugin = require("html-webpack-plugin");

var APP_DIR = path.resolve(__dirname, "src");

var config = {
  entry: `${APP_DIR}/index.js`,
  output: {
    path: path.resolve(__dirname, 'build'),
    filename: 'index_bundle.js',
    publicPath: '/gnf/user/offboarding',
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader"
        }
      },
      {
        test: /\.html$/,
        exclude: [/node_modules/],
        use: [
          {
            loader: "html-loader"
          }
        ]
      },
      {
        test: /\.(s*)css$/,
        use: ["style-loader", "css-loader", "sass-loader"]
      },
    ]
  },
  plugins: [
    new HtmlWebPackPlugin({
      // template: `${APP_DIR}/index.html`,
      // favicon: `${APP_DIR}/assets/img/logo.png`
      template: path.resolve(__dirname, 'src/', 'index.html'),
      //favicon: `${APP_DIR}/assets/img/logo.png`,
      favicon: path.resolve(__dirname, 'src/assets/img', 'logo.png'),
      filename: 'index.html'
    }),
    new webpack.HotModuleReplacementPlugin()
  ],
  devServer: {
    historyApiFallback: true,
    hot: true
  }
};

module.exports = config;
